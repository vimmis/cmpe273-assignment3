import bisect
import md5
import json
import requests

class ConsistentHashRing(object):
    """Implement a consistent hashing ring."""

    def __init__(self):
        #Create a new ConsistentHashRing.

        self._keys = []
        self._nodes = {}

    def _hash(self, key):
        """Given a string key, return a hash value."""

        return long(md5.md5(key).hexdigest(), 16)

    def _repl_iterator(self, nodename):
        """Given a node name, return an iterable of replica hashes."""

        return (self._hash(nodename))
                
    def __setitem__(self, nodename, node):
        """Add a node, given its name."""
        hash_ = self._repl_iterator(nodename)
        if hash_ in self._nodes:
        	raise ValueError("Node name %r is "
                            "already present" % nodename)
        self._nodes[hash_] = node
        bisect.insort(self._keys, hash_)

    def __delitem__(self, nodename):
        """Remove a node, given its name."""

        for hash_ in self._repl_iterator(nodename):
            # will raise KeyError for nonexistent node name
            del self._nodes[hash_]
            index = bisect.bisect_left(self._keys, hash_)
            del self._keys[index]

    def __getitem__(self, key):

        hash_ = self._hash(key)
        start = bisect.bisect(self._keys, hash_)
        if start == len(self._keys):
            start = 0
        return self._nodes[self._keys[start]]

data=json.load(open('request.json'))

ring = ConsistentHashRing()
nodes = {"127.0.0.1:3000", "127.0.0.1:4000", "127.0.0.1:5000"}
for i in nodes :	
	ring[i] = i
#POST
print 'POSTING Requests\n'
for d in data:
	port= ring[d["id"]]
	url = 'http://' + port+ '/v1/expenses'
	r = requests.post(url, json.dumps(d))
	json_response = json.loads(r.content)
	print 'Order ID:{} --> server:{}'.format(d["id"],port)

#GET
print 'GETING Requests\n'
for d in data:
	port= ring[d["id"]]
	url = 'http://' + port+ '/v1/expenses/'+d["id"]
	r = requests.get(url)
	json_response = json.loads(r.content)
	print 'Order ID:{} --> server:{}'.format(d["id"],port)
	print 'data: {}\n'.format(json_response)

