import datetime, sqlalchemy.ext.declarative, sqlalchemy
from flask import Flask, jsonify, request
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.exc import SQLAlchemyError
from marshmallow import Schema, fields
from validate_email import validate_email
import simplejson as json
import time

#for allowing mysqlserver to get created and running before app
#time.sleep(20)

app = Flask(__name__)

DATABASE = 'db'
PASSWORD = 'Chetu1234'
USER = 'root'
HOSTNAME = 'mysql2'

engine = sqlalchemy.create_engine('mysql://%s:%s@%s'%(USER, PASSWORD, HOSTNAME)) # connect to server
engine.execute("CREATE DATABASE IF NOT EXISTS %s "%(DATABASE)) #create db

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@%s:3306/%s'%(USER, PASSWORD, HOSTNAME, DATABASE)
db = SQLAlchemy(app)

# Data Model User Table
class Expense(db.Model):

	__tablename__ = 'expense'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100))
	email = db.Column(db.String(120))
	category=db.Column(db.String(120))
	description=db.Column(db.Text)
	link=db.Column(db.String(120))
	estimated_costs=db.Column(db.String(100))
	submit_date=db.Column(db.String(10))
	status=db.Column(db.String(100))
	decision_date=db.Column(db.String(10))

	def __init__(self, idd,name, email, category,description,link,estimated_costs,submit_date,status,decision_date):
		# initialize columns
		self.id = idd
		self.name = name
		self.email = email
		self.category = category
		self.description = description
		self.link = link
		self.estimated_costs = estimated_costs
		self.submit_date = submit_date
		self.status = status
		self.decision_date = decision_date

	def __repr__(self):
		return '<User %r>' % self.name
#SCHEMA

class ExpenseSchema(Schema):
	id = fields.Int(dump_only=False)
	name = fields.Str()
	email = fields.Email()
	category = fields.Str()
	description = fields.Str()
	link= fields.Str()
	estimated_costs = fields.Str()
	submit_date = fields.Str()
	status = fields.Str()
	decision_date = fields.Str() 

		
expense_schema = ExpenseSchema()

#table creation
db.create_all()

@app.route('/')
@app.route('/v1/expenses')
def index():
	return 'Hello World! Docker-Compose for Flask & Mysql app2'

@app.route("/v1/expenses/<int:id>")
def get_expense(id):
    expense_values = Expense.query.get(id)
    if not expense_values:
        return jsonify({"message": "Expense Id could not be found."}), 404
    expense_result = expense_schema.dump(expense_values)
    return jsonify(expense_result.data)

@app.route("/v1/expenses", methods=["POST"])
def new_expense():
	data = request.data
	dataDict = json.loads(data)
	if not dataDict:
		return jsonify({'message': 'No input data provided'}), 400
  
  # Validating input

	# to maintain a dictionary of valid keys and values with null as default 
	dict2 = {'id':'', 'name':'','email':'','category':'','description':'','link':'','estimated_costs':'','submit_date':''}

	if 'category' in dataDict:
		if(dataDict['category']== 'office supplies' or dataDict['category']=='travel' or dataDict['category']=='training'):
			dict2['category']=dataDict['category']
		else:
			return "Invalid category",400 
		
	if 'estimated_costs' in dataDict:
		try:
			local=float(dataDict['estimated_costs'])
		except:
			return "Invalid estimated cost ",400
		dict2['estimated_costs']= dataDict['estimated_costs']
		
	if 'submit_date' in dataDict:
		try:
			local=(datetime.datetime.strptime(dataDict['submit_date'], '%m-%d-%Y')).strftime('%m-%d-%Y')
		except:	
			return "Invalid submit date",400
		dict2['submit_date']= dataDict['submit_date']
		
	if 'name' in dataDict:
		dict2['name'] = dataDict['name']

	if 'email' in dataDict:
		if validate_email(dataDict['email']):
			dict2['email'] = dataDict['email']
		else:
			return "Invalid email", 400

	if 'description' in dataDict:
		dict2['description'] = dataDict['description']

	if 'link' in dataDict:
		dict2['link'] = dataDict['link']	
	if 'id' in dataDict:
		try:
			tempid = int(dataDict['id'])
		except:
			return "Invalid id", 400
		dict2['id'] = int(dataDict['id'])
	
		 
    # Create new expense row
	
	expense = Expense(
	dict2['id'],
        dict2['name'],
       	dict2['email'],
        dict2['category'],
	dict2['description'],
	dict2['link'],
	dict2['estimated_costs'],
	dict2['submit_date'],
	"pending", # status is kept pending and decision date as null
	"")
	
	try:	
		db.session.add(expense)
		db.session.commit()
	except SQLAlchemyError as f:
		db.session.rollback()
		resp = jsonify({"error": str(f)})
		resp.status_code = 401
		return resp
		
	result = expense_schema.dump(Expense.query.get(expense.id))
	return jsonify(result.data),201

@app.route("/v1/expenses/<int:id>",methods=["DELETE"])
def delete(id):
        order = Expense.query.get(id)
	if not order:
       		return jsonify({"message": "Expense Id could not be found."}), 404
        try:
        	db.session.delete(order)
		db.session.commit()
		return ('', 204)
		 
        except SQLAlchemyError as e:
                db.session.rollback()
                resp = jsonify({"error": str(e)})
                resp.status_code = 401
                return resp

@app.route("/v1/expenses/<int:id>",methods=["PUT"])
def post(id):
        order = Expense.query.get(id)
	if not order:
       		return jsonify({"message": "Expense Id could not be found."}), 404
        else:
		data2 = request.data
		dataDict2 = json.loads(data2)        	
		if not dataDict2:
			return jsonify({'message': 'No input data provided'}), 400

  # Validating input


	if 'category' in dataDict2:
		if (dataDict2['category']== 'office supplies' or dataDict2['category']=='travel' or dataDict2['category']=='training'):
			setattr(order,'category',dataDict2['category'])
		else:
			return ("Invalid category"),400

	if 'estimated_costs' in dataDict2:
		try:
			float(dataDict2['estimated_costs'])
		except:
			return ("Invalid estimated cost"),400
		setattr(order,'estimated_costs',dataDict2['estimated_costs'])
		
	if 'submit_date' in dataDict2:
		try:
			local=(datetime.datetime.strptime(dataDict2['submit_date'], '%m-%d-%Y')).strftime('%m-%d-%Y')
		except:	
			return ("Invalid submit date"),400
		setattr(order,'submit_date',dataDict2['submit_date'])
		
	if 'name' in dataDict2:
		setattr(order,'name',dataDict2['name'])

	if 'email' in dataDict2:
		setattr(order,'email',dataDict2['email'])

	if 'description' in dataDict2:
		setattr(order,'description', dataDict2['description'])

	if 'link' in dataDict2:
		setattr(order, 'link',dataDict2['link'])

	if 'id' in dataDict2:
		return("Id  change not allowed"),400	

	if 'status' in dataDict2: # allow only status to be rejected , rest invalid request
		if dataDict2['status'] == 'rejected':
			setattr(order, 'status', dataDict2['status'])
		else:
			return("status change not allowed except for 'rejected'"),400	
	
	if 'decision_date' in dataDict2:
		try:
			local=(datetime.datetime.strptime(dataDict2['decision_date'], '%m-%d-%Y')).strftime('%m-%d-%Y')
		except:	
			return ("Invalid decision date"),400
		setattr(order,'decision_date',dataDict2['decision_date'])	
	
	try:
	       	db.session.commit()
		return ('', 202)
	except SQLAlchemyError as f:
		db.session.rollback()
		resp = jsonify({"error": str(f)})
		resp.status_code = 401
		return resp
	
if __name__ == "__main__":
	engine = sqlalchemy.create_engine('mysql://%s:%s@%s'%(USER, PASSWORD, HOSTNAME)) # connect to server
	engine.execute("CREATE DATABASE IF NOT EXISTS %s "%(DATABASE)) #create db
	app.run(host="0.0.0.0", port=4000, debug=True)
