Assignment requirements are below:
https://github.com/sithu/cmpe273-fall16/tree/master/assignment3

Submission:

1. To create 3 differnt mysql containers(pls change pwd/path according to your env,ensuring volume paths different):

docker run --name mysql1 -e MYSQL_ROOT_PASSWORD="Chetu1234" -v /home/vimmis/cmpe273-assignment3/db1:/var/lib/mysql -d mysql:latest  
docker run --name mysql2 -e MYSQL_ROOT_PASSWORD="Chetu1234" -v /home/vimmis/cmpe273-assignment3/db2:/var/lib/mysql -d mysql:latest  
docker run --name mysql3 -e MYSQL_ROOT_PASSWORD="Chetu1234" -v /home/vimmis/cmpe273-assignment3/db3:/var/lib/mysql -d mysql:latest  

The folders db1,db2,db3 will be made on runing above commands.

2. app1,app2, app3 folders  are each for a server  
Kindly go in each folder to build and run docker ,please change mysql pwd for your env in all 3 app.py.
Make sure the each app links to diffrnt mysql ,for convenience, app1-mysql1,app2-mysql2 etc.

app1:
docker build -t app1 .  
docker run --name app1 --link mysql1 -p 3000:3000 app1

app2:
docker build -t app2 .  
docker run --name app2 --link mysql2 -p 4000:4000 app2

app3:
docker build -t app3 .  
docker run --name app3 --link mysql3 -p 5000:5000 app3  

Please note, 'docker-compose up' can be run if 'docker run' is not to be used, as no other server is present except the app itself in yml.

3. Once servers are up and running,run the ch_client.py. Please note, the 10 request json are in request.json file:  
python ch_client.py

TO verify, print statements are used to output order id and port mapping in both GET and POST

Snapshots:

assg3POST.png : provides output of POST section after running ch_client.py  
assg3GET.png: provides output of GET section after running ch_client.py  
app1-3000-output.png,app2-4000-output.png,app3-5000-output.png : first section(above '------') provides the log from app when ch_client.py was run. 
The second section(below '-----') shows log when the URL was hit on browser for all orders GET. 
The two sections can be compared for verifying results for load distribution,sharding.
